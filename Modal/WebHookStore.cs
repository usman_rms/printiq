using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Sampleapi.Modal

{
    public class WebHookStore
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]

        public string Id { get; set; }
        public string WebbHookKey { get; set; }

   
        public string WebbHooValue { get; set; }
    }
}
