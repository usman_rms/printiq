﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Sampleapi.Core;
using Sampleapi.Modal;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Sampleapi.Controllers
{
    [Route("webhook/[controller]")]
    [ApiController]
    public class HookController : ControllerBase
    {
        // GET: api/<HookController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<HookController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<HookController>
        [HttpPost]
        public ActionResult Post([FromBody] WebHookStoreDTO webbhook)
        {
            MongoConnect db = new MongoConnect();
            var _webhook = db.getDB().GetCollection<BsonDocument>("webhook");

            var _model = new WebHookStore();
            _model.WebbHookKey = webbhook.WebbHookKey;
            _model.WebbHooValue = webbhook.WebbHooValue;
            
            _webhook.InsertOne(webbhook.ToBsonDocument());
            return   Ok(new
            {
                KeyHook = webbhook.WebbHookKey,
                ValueHook = webbhook.WebbHooValue,
            });
        }

        // PUT api/<HookController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<HookController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
